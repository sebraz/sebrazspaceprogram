package com.bitbucket.sebraz.spacerace.init;

import com.bitbucket.sebraz.spacerace.command.CommandExit;
import com.bitbucket.sebraz.spacerace.command.CommandGo;
import com.bitbucket.sebraz.spacerace.command.CommandHelp;
import com.bitbucket.sebraz.spacerace.command.ICommand;
import com.bitbucket.sebraz.spacerace.lib.Reference;

import java.util.ArrayList;

/**
 *
 */
public class Commands
{
	public static final ArrayList<ICommand> commands = new ArrayList<>();

	public static void init()
	{
		commands.add(new CommandGo());
		commands.add(new CommandHelp());
		commands.add(new CommandExit());
	}

	public static boolean executeCommand(String commandName, String[] args)
	{
		if (args == null)
			args = new String[]{};

		for (ICommand command : commands)
		{ // Look for a match in all the names of all the commands
			for (String name : command.getCommandNames())
			{
				if (commandName.equalsIgnoreCase(name))
				{
					command.executeCommand(args);
					return true;
				}
			}
		}
		return false;
	}
}

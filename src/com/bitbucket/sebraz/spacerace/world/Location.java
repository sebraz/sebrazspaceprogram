package com.bitbucket.sebraz.spacerace.world;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Location
{

	private final String id;
	private transient Location parent;
	private transient final ArrayList<Location> children = new ArrayList<>();

	public Location(String id)
	{
		this.id = id;
	}

	public String getId()
	{
		return id;
	}

	public Location getParent()
	{
		return parent;
	}

	public void setParent(Location parent)
	{
		if (parent != null)
			parent.children.remove(this);
		this.parent = parent;
		parent.children.add(this);
	}

	public Location addChild(Location child)
	{
		for (Location location = child; location != null; location = location.parent)
			if (location == this)
				throw new IllegalArgumentException(String.format("Setting %s as a child of %s would cause a cyclic reference", child, this));

		child.setParent(this);
		return this;
	}

	public ArrayList<Location> getChildren()
	{
		return children;
	}

	public List<Location> getPath()
	{
		ArrayList<Location> locations = new ArrayList<>();

		for (Location location = this; location != null; location = location.parent)
			locations.add(0, location);

		return locations;
	}

	@Override
	public String toString()
	{
		return this.getId();
	}
}

package com.bitbucket.sebraz.spacerace;

import com.bitbucket.sebraz.spacerace.command.CommandExit;
import com.bitbucket.sebraz.spacerace.init.Commands;
import com.bitbucket.sebraz.spacerace.init.Locations;
import com.bitbucket.sebraz.spacerace.lib.Reference;
import com.bitbucket.sebraz.spacerace.player.Player;
import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class SpaceRace
{

	public static void main(String[] args) throws IOException
	{
		Locations.init();
		Commands.init();

		Player.localPlayer.location = Locations.root;

		while (!CommandExit.shouldExit())
		{

			Reference.writer.printf("%s>", Joiner.on('>').join(Player.localPlayer.location.getPath()));

			List<String> splitInput = Splitter.on(CharMatcher.INVISIBLE).omitEmptyStrings().splitToList(Reference.reader.readLine());

			// User didn't enter text: Don't do anything
			if (splitInput.isEmpty())
				continue;

			Reference.writer.println();

			String command = splitInput.get(0);

			ArrayList<String> commandArgs = new ArrayList<>(splitInput);
			commandArgs.remove(0);

			if (!Commands.executeCommand(command, commandArgs.toArray(new String[commandArgs.size()])))
				Reference.writer.printf(" '%s' is not a valid command%n", command);

			Reference.writer.println();
		}
		Reference.writer.printf(" Bye!%n");
	}
}

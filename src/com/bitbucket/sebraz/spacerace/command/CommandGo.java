package com.bitbucket.sebraz.spacerace.command;

import com.bitbucket.sebraz.spacerace.lib.Reference;
import com.bitbucket.sebraz.spacerace.player.Player;
import com.bitbucket.sebraz.spacerace.world.Location;
import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.util.Arrays;

/**
 *
 */
public class CommandGo implements ICommand
{
	@Override
	public String[] getCommandNames()
	{
		return new String[]{"go"};
	}

	@Override
	public void executeCommand(String[] args)
	{
		Player player = Player.localPlayer;
		switch (args.length)
		{
			case 0:
				if (player.location.getParent() == null)
				{
					// Root

					if (player.location.getChildren().isEmpty())
					{
						// Dead end

						Reference.writer.printf(" You don't seem able to go anywhere...%n");
						Reference.writer.printf(" I swear that's is not supposed to happen :^/%n");
					}
					else
					{
						// Has children

						Reference.writer.printf(" You can:%n");
						Reference.writer.printf("  Go to%n");
						for (Location location : player.location.getChildren())
							Reference.writer.printf("   %s%n", location.getId());
					}
				}
				else
				{
					// Not Root

					if (player.location.getChildren().isEmpty())
					{
						// Dead end

						Reference.writer.printf(" You can:%n");
						Reference.writer.printf("  Go back%n");

					}
					else
					{
						// Has children

						Reference.writer.printf(" You can:%n");
						Reference.writer.printf("  Go back or%n");
						Reference.writer.printf("  Go to%n");
						for (Location location : player.location.getChildren())
							Reference.writer.printf("   %s%n", location.getId());
					}
				}

				break;

			default:
				switch (args[0])
				{
					case "back":
						if (player.location.getParent() == null)
							Reference.writer.printf(" You can't go further back!%n");
						else
							player.location = player.location.getParent();
						break;
					case "to":
						if (args.length >= 2)
						{
							String dest = Joiner.on(' ').join(Arrays.copyOfRange(args, 1, args.length));

							for (Location location : player.location.getChildren())
								if (dest.equalsIgnoreCase(location.getId()) ||
										dest.equalsIgnoreCase(StringUtils.deleteWhitespace(WordUtils.initials(location.getId()))))
								{
									player.location = location;
									return;
								}

							Reference.writer.printf(" '%s' is not accesible from here%n", dest);
						}
						else
						{
							Reference.writer.printf("  Go to where?%n");
						}
				}
		}
	}
}

package com.bitbucket.sebraz.spacerace.command;

import com.bitbucket.sebraz.spacerace.init.Commands;
import com.bitbucket.sebraz.spacerace.lib.Reference;
import com.google.common.base.Joiner;

/**
 *
 */
public class CommandHelp implements ICommand
{

	@Override
	public String[] getCommandNames()
	{
		return new String[]{"help"};
	}

	@Override
	public void executeCommand(String[] args)
	{
		for (ICommand command : Commands.commands)
			Reference.writer.printf(" %s%n", Joiner.on('/').join(command.getCommandNames()));
	}
}

package com.bitbucket.sebraz.spacerace.command;

import com.bitbucket.sebraz.spacerace.lib.Reference;

/**
 *
 */
public class CommandExit implements ICommand
{
	@Override
	public String[] getCommandNames()
	{
		return new String[]
				{
						"exit",
						"quit"
				};
	}

	@Override
	public void executeCommand(String[] args)
	{
		exit = true;
	}

	private static boolean exit;

	public static boolean shouldExit()
	{
		return exit;
	}
}

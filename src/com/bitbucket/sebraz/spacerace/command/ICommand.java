package com.bitbucket.sebraz.spacerace.command;

/**
 *
 */
public interface ICommand
{
	String[] getCommandNames();

	void executeCommand(String[] args);
}

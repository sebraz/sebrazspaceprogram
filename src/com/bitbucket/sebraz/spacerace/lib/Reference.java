package com.bitbucket.sebraz.spacerace.lib;

import com.google.common.io.CharSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 *
 */
public class Reference
{
	public static final Gson gson = new GsonBuilder()
			.setPrettyPrinting()
			.serializeNulls()
			.create();

	public static final File DATAFILE = new File("run/data.txt");
	public static final Charset CHARSET = StandardCharsets.UTF_8;

//	public static Console console = System.console();

	public static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	public static final PrintWriter writer = new PrintWriter(System.out, true);
}

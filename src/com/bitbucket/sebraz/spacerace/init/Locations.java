package com.bitbucket.sebraz.spacerace.init;

import com.bitbucket.sebraz.spacerace.world.Location;

/**
 *
 */
public class Locations
{

	public static Location root;

	public static void init()
	{
		root = new Location("Space Center")
				.addChild(
						new Location("Astronaut Complex")
								.addChild(
										new Location("Mercury Program")
								)
								.addChild(
										new Location("Gemini Program")
								)
								.addChild(
										new Location("Apollo Program")
								)
								.addChild(
										new Location("Shuttle Program")
								)
								.addChild(
										new Location("Orion Program")
								)
				)
				.addChild(
						new Location("Research & Development")
								.addChild(
										new Location("Prototype Purchasing Facility")
								)
								.addChild(
										new Location("Prototype Development")
								)
				)
				.addChild(
						new Location("Vehicle Assembly Building")
				)
				.addChild(
						new Location("Administration")
				)
				.addChild(
						new Location("Launch Complex")
				)
		;
	}
}
